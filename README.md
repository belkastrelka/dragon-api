# Dragon API 
![Dragon IMG](http://ispaceup.dragon.family/static/favicon.ico)


## 1 Работа с профилем и семьей

----------

Система авторизации основана на поддоменах. При регистрации для каждой семьи создается уникальный поддомен - логин семьи. Каждый пользователь авторизуется в системе с помощью логина и пароля. Логин должен быть уникальным в пределах поддомена. 

### 1.1 Регистрация
В регистрации идет создание семьи и ее главного члена.

* **URL**
	/auth/register/

* **Тип&Метод**
	`application/json & POST`
  
*  **URL параметры**
	Нет

* **Авторизованность**
	Нет

* **Параметры запроса** 
	**Обязательные:** 
	* `email=[string]` - почта главного;
	* `username=[string]` - логин главного;
	* `password=[string]` - пароль главного;
	* `first_name=[string]` - имя главного;
	* `last_name=[string]` - фамилия главного;
	* `balance=[decimal]` - баланс главного;
	* `role=[integer]` - роль главного (1-Папа/2-Мама);
	* `family=[object]` - создаваемая семья, состоящая из следующих параметров:
		* `domain=[string]` - уникальный домен семьи;
		* `name=[string]` - имя семьи;
		* `ratio=[integer]` - коэффициент перерасчета рублей в дракончики.

	**Опциональные:**
	* `phone=[string]` - номер телефона главного в формате `7xxxxxxxxxx`;
	* `date_of_birth=[string]` - дата рождения главного в формате `YYYY-MM-DD`.

* **Пример запроса**

		{
			"email": "test@dragon.ispaceup.com",
			"username": "test",
			"password":"qweqwe",
			"first_name": "Тест",
			"last_name": "Тестов",
			"balance": 10000,
			"phone": "79267054473",
			"role": 1,
			"date_of_birth": "1992-10-13",
			"family": {
				"domain": "test",
				"name": "Тестовы",
				"ratio": 10
			}
		}

* **Параметра ответа**
	* `id=[integer]` - `ID` созданного главного;
	* `email=[string]` - почта главного;
	* `username=[string]` - логин главного;
	* `first_name=[string]` - имя главного;
	* `last_name=[string]` - фамилия главного;
	* `phone=[string]` - номер телефона главного в формате `7xxxxxxxxxx`;
	* `balance=[decimal]` - баланс главного;
	* `role=[integer]` - роль главного (1-Папа/2-Мама);
	* `date_of_birth=[string]` - дата рождения главного в формате `YYYY-MM-DD`;
	* `is_principal=[boolean]` - является ли пользователем главным;
	* `token=[string]` - сессионный токен;

* **Пример ответа**

		{
			"status": true,
			"data": {
				"id": 1,
				"email": "test@dragon.ispaceup.com",
				"username": "test:test",
				"first_name": "Тест",
				"last_name": "Тестов",
				"phone": "79267054473",
				"role": 1,
				"date_of_birth": "1992-10-13",
				"is_principal": true,
				"token": "da2e656f5c8905dc25345ef1b2b90a2558af786d"
			}
		}

* **Заметки**
	1. В ответе регистрации приходит сессионный токен, с которым пользователем считается авторизованным в системе.




### 1.2 Авторизация

* **URL**
	/auth/auth/

* **Тип&Метод**
	`form-data & POST`
  
*  **URL параметры**
	Нет

* **Авторизованность:**
	Нет

* **Параметры запроса** 
	**Обязательные:** 
	* `username=[string]` - логин пользователя;
	* `password=[string]` - пароль пользователя;
	* `domain=[string]` - уникальный домен семьи;

* **Пример запроса**

		POST /auth/login/ HTTP/1.1
		Host: dragon.ispaceup.com
		Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

		------WebKitFormBoundary7MA4YWxkTrZu0gW
		Content-Disposition: form-data; name="username"

		test
		------WebKitFormBoundary7MA4YWxkTrZu0gW
		Content-Disposition: form-data; name="password"

		passwd
		------WebKitFormBoundary7MA4YWxkTrZu0gW
		Content-Disposition: form-data; name="domain"

		test
		------WebKitFormBoundary7MA4YWxkTrZu0gW--


* **Параметра ответа**
	* `token=[string]` - сессионный токен;

* **Пример ответа**

		{
			"status": true,
			"data": {
				"token": "d3b6d24559f0e33dba6d57e232b8f27e0e798131"
			}
		}




### 1.3 Разавторизация

* **URL**
  /auth/auth/

* **Тип&Метод**
  `form-data & POST`
  
*  **URL параметры**
	Нет

* **Авторизованность**
	Да

* **Параметры запроса** 
	Нет

* **Пример запроса**

		POST /auth/logout/ HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token d3b6d24559f0e33dba6d57e232b8f27e0e798131

* **Параметра ответа**
	Нет

* **Пример ответа**

		{
			"status": true
		}




### 1.4 Профиль

* **URL**
  /profile/

* **Тип&Метод**
  `GET`
  
*  **URL параметры**
	Нет

* **Авторизованность**
	Да

* **Параметры запроса** 
	Нет

* **Пример запроса**

		GET /profile/ HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token 5cfb80b47ee2169ab56094a155fed85a9147c49a

* **Параметра ответа**
	* `id=[integer]` - `ID` пользователя;
	* `username=[string]` - логин пользователя;
	* `first_name=[string]` - имя пользователя;
	* `last_name=[string]` - фамилия пользователя;
	* `role=[object]` - роль пользователя, состоящая из следующих параметров:
		* `id=[integer]` - `ID` роли в системе;
		* `nominative=[string]` - роль в именительном падеже;
		* `genitive=[string]` - роль в родительном падеже;
		* `type=[string]` - тип роли (**a**-взрослая/**c**-десткая);
		* `gender=[string]` - пол роли (**M**-мужская/**F**-детская);
	* `image_detail=[object]` - объект аватарки пользователя;
		* `id=[integer]` - `ID` изображения;
		* `image=[string]` - абсолютный путь до оригинала изображения;
		* `thumbnail=[string]` - абсолютный путь до обработанного изображения;
		* `created=[string]` - дата загрузки изображения в формате `YYYY-MM-DDThh:mm:ss`;
	* `child_pofile=[object]` - профиль ребенка;
		* `id=[integer]` - `ID` ребенка;
		* `name=[string]` - название профиля;
	* `date_of_birth=[string]` - дата рождения пользователя в формате `YYYY-MM-DD`;
	* `age=[integer]` - возраст;
	* `balance=[decimal]` - количество имеющихся дракончиков.

* **Пример ответа**

		{
			"status": true,
			"data": {
				"id": 15,
				"username": "test_son",
				"first_name": "Ванюша",
				"last_name": "Тестов",
				"role": {
					"id": 3,
					"nominative": "Сын",
					"genitive": "Сына",
					"gender": "M",
					"type": "c"
				},
				"image_detail": {
					"id": 17,
					"image": "http://dragon.ispaceup.com/images/..."jpg,
					"thumbnail": "http://dragon.ispaceup.com/images/..thumb.jpg?v=1505138971",
					"created": "2017-09-11T14:09:31.489358"
				},
				"date_of_birth": "2010-04-01",
				"age": 7,
				"balance": "0.00",
				"child_profile": {
					"id": 2,
					"name": "Гений"
				}
			}
		}




### 1.5 Информация о семье
* **URL:**
	/family/

* **Тип&Метод**
	`GET`
  
*  **URL параметры**
	Нет

* **Авторизованность**
	Да

* **Параметры запроса** 
	Нет

* **Пример запроса**

		GET /family/ HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token 8bc06cfb846e69ea430ced7ddea641f1827666e9

* **Параметра ответа**
	* `id=[integer]` - `ID` семьи;
	* `name=[string]` - имя семьи;
	* `ratio=[integer]` - коэффициент перерасчета рублей в дракончики;
	* `domain=[string]` - уникальный домен семьи;

* **Пример ответа**

		{
			"status": true,
			"data": {
				"id": 1,
				"name": "Паки",
				"ratio": 20,
				"library_ratio": 1,
				"domain": "pak"
			}
		}




### 1.6 Члены семьи
* **URL**
	/family/

* **Тип&Метод:**
	`GET`
  
*  **URL параметры**
	Нет

* **Авторизованность:**
	Да

* **Параметры запроса** 
	Нет

* **Пример запроса**

		GET /family/ HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token 8bc06cfb846e69ea430ced7ddea641f1827666e9

* **Параметра ответа**
	* `children=[list]` - список детей;
	* `adults=[list]` - список взрослых.

* **Пример ответа**

		{
			"status": true,
			"data": {
				"children": [
					{
						"id": 3,
						"username": "son",
						"first_name": "Сын",
						"last_name": "Пак",
						"role": {
							...
						},
						"image_detail": {
							...
						},
						...
					},
					{
						"id": 4,
						...
					}
				],
				"adults": [
					{
						"id": 1,
						"username": "dad",
						"first_name": "Папа",
						"last_name": "Пак",
						"role": {
							...
						},
						"image_detail": {
							...
						},
						...
					},
					{
						"id": 2,
						...
					},
				]
			}
		}




## 2 Управление заданиями

----------

### 2.1 Список заданий
* **URL**
  /auth/auth/

* **Тип&Метод:**
  `form-data & POST`
  
*  **URL параметры**
	Нет

* **Авторизованность:**
	Да

* **Параметры запроса** 
	**Необязательные:**
	* `creators=[string]` - `ID` создателей заданий через `,`;
	* `trends=[string]` - `ID` подкатегорий через `,`;
	* `amount_0=[integer]` - нижняя граница стоимости задания;
	* `amount_1=[integer]` - верхняя граница стоимости задания;
	* `date_0=[string]` - нижняя граница даты в формате `DD.MM.YYYY`;
	* `date_1=[string]` - верхняя граница даты в формате `DD.MM.YYYY`;
	* `kinds=[string]` - тип задания:
		* `1` - без даты;
		* `2` - календарное;
		* `3` - периодическое;
		* `4` - с дедлайном;
		* `5` - мгновенное;
		* `6` - запись к врачу.

* **Пример запроса**

		GET /tasks/?creators=1,2&trends=1,2&amount_0=0&amount_1=100&date_0=01.06.2016&date_1=13.08.2018&kinds=1,2 HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token 8bc06cfb846e69ea430ced7ddea641f1827666e9


* **Параметра ответа**
	* `count=[integer]` - количество заданий;
	* `tasks=[list]` - список заданий:
		* `id=[integer]` - `ID` задания;
		* `creator=[object]` - создатель задания;
		* `executor=[object]` - исполнитель задания;
		* `created_dt=[string]` - дата и время создания задания в формате `YYYY-MM-DDThh:mm:ss`;
		* `executed_dt=[string]` - дата и время выполнения задания в формате `YYYY-MM-DDThh:mm:ss`;
		* `checked_dt=[string]` - дата и время проверки задания в формате `YYYY-MM-DDThh:mm:ss`;
		* `description=[string]` - описание задания;
		* `trend=[object]` - подкатегория задания:
			* `id=[integer]` - `ID` подкатегории;
			* `name=[string]` - название подкатегории;
		* `status=[ingeter]` - статус задания:
			* `0` - создано;
			* `1` - выполнено;
			* `2` - проверено;
		* `amount=[integer]` - сумма дракончиков;
		* `place=[string]` - место выполнения задания;
		* `address=[string]` - адрес выполнения задания;
		* `task_type=[object]` - тип задания:
			* `id=[integer]` - `ID` типа заданий;
			* `name=[string]`- имя типа заданий;
			* `amount_required=[boolean]`- необходимость ввода суммы дракончиков;
			* `color=[string]` - цвет типа заданий.

* **Пример ответа**

		{
			"status": true,
			"data": {
				"count": 32,
				"tasks": [
					{
						"id": 35,
						"creator": {
							"id": 1,
							"username": "dad",
							"first_name": "Папа",
							"last_name": "Пак",
							...
						},
						"executor": {
							"id": 3,
							"username": "son",
							"first_name": "Сын",
							"last_name": "Пак",
							...
						},
						"created_dt": "2017-09-04T14:30:00",
						"executed_dt": "2017-09-05T08:30:00",
						"checked_dt": null,
						"description": "Сделать домашнее задание",
						"trend": {
							"id": 1,
							"name": "Физика"
						},
						"status": 2,
						"amount": 50,
						"place": null,
						"address": null,
						"task_type": {
							"id": 1,
							"name": "Ручной ввод",
							"amount_required": true,
							"color": "B74C4D"
						},
					},
					{
						"id": 36,
						...
					},
					...
				]
			}
		}




### 2.2 Выполнение задания
* **URL**
  child/tasks/:id/

* **Тип&Метод:**
  `form-data & POST`
  
*  **URL параметры**
	* `id=[integer]` - `ID` выполняемого задания.

* **Авторизованность:**
	Да

* **Параметры запроса** 
	Нет

* **Пример запроса**

		POST /child/tasks/39/ HTTP/1.1
		Host: dragon.ispaceup.com
		Authorization: Token a3c2969aa02f61b34c75b2a5b5c331276a51e818




* **Параметра ответа**
	Нет

* **Пример ответа**

		{
			"status": true
		}
